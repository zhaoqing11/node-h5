module.exports = {
  publicPath: './',
  devServer: {
    // 设置代理
    proxy: {
      '/api': {
        target: 'https://mock.apifox.cn/m1/2681883-0-default',
        ws: true, // 是否启用websockets
        changeOrigin: true, // 开启代理
        pathRewrite: {
          // 重写代理规则 /api开头 代理到/
          '^/api': '/'
          // 例：/api/login代理到
          // http://yapi.xx.cn/mock/login
        }
      }
    },
    // 内网穿透 生产环境 开启可IP和域名访问权限
    historyApiFallback: true,
    allowedHosts: 'all'
    // disableHostCheck: true // 绕过主机检查
  }
  // chainWebpack: config => {
  //   config.devServer.disableHostCheck(true)
  // }
}
