// 日期格式化
export function formatDate(time) {
  var date = new Date(time)
  var len = time.toString().length
  if (len < 13) {
    var sub = 13 - len
    sub = Math.pow(10, sub)
    date = new Date(time * sub)
  }
  var y = date.getFullYear()
  var M = date.getMonth() + 1
  M = M < 10 ? '0' + M : M
  var d = date.getDate()
  d = d < 10 ? '0' + d : d
  var h = date.getHours()
  h = h < 10 ? '0' + h : h
  var m = date.getMinutes()
  m = m < 10 ? '0' + m : m
  var s = date.getSeconds()
  s = s < 10 ? '0' + s : s
  return y + M + d + h + m + s
}

// 生成4位随机数
export function createCode() {
  var code = ''
  var codeLength = 4
  var random = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
  for (var i = 0; i < codeLength; i++) {
    var index = Math.floor(Math.random() * 9)
    code += random[index]
  }
  return code
}
