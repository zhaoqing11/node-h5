import request from '@/utils/request'

/**
 * 登录
 * @param {*} data
 */
export const login = data => {
  return request.post('/login', data)
}

/**
 * 获取用户信息
 * @param {*} data
 */
export const userInfo = data => {
  return request.post('/user/info', data)
}

export const test = () => {
  return request.get('/test')
}
